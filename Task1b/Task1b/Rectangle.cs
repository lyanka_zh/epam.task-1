﻿using System;

namespace Task1b
{
    public enum Direction
    {
        Up,
        Down,
        Right,
        Left
    }

    public class Rectangle
    {
        private Point _leftBottomPoint;
        private Point _rightTopPoint;

        public Rectangle()
        {
        }

        public Rectangle(Point leftBottomPoint, Point rightTopPoint)
        {
            _leftBottomPoint = leftBottomPoint;
            _rightTopPoint = rightTopPoint;
        }

        public Rectangle(double x1, double y1, double x2, double y2) : this(new Point(x1, y1), new Point(x2, y2))
        {
        }

        public Point LeftBottomPoint
        {
            get { return _leftBottomPoint; }
            set { _leftBottomPoint = value; }
        }

        public Point RightTopPoint
        {
            get { return _rightTopPoint; }
            set { _rightTopPoint = value; }
        }

        public void Move(double delta, Direction direction)
        {
            if (direction == Direction.Down)
            {
                _leftBottomPoint.Y -= delta;
                _rightTopPoint.Y -= delta;
            }
            else if (direction == Direction.Up)
            {
                _leftBottomPoint.Y += delta;
                _rightTopPoint.Y += delta;
            }
            else if (direction == Direction.Left)
            {
                _leftBottomPoint.X -= delta;
                _rightTopPoint.X -= delta;
            }
            else if (direction == Direction.Right)
            {
                _leftBottomPoint.X += delta;
                _rightTopPoint.X += delta;
            }
        }

        public void SideScale(double scaleParametr)
        {
            _rightTopPoint.X = _leftBottomPoint.X + (_rightTopPoint.X - _leftBottomPoint.X) * scaleParametr;
            _rightTopPoint.Y = _leftBottomPoint.Y + (_rightTopPoint.Y - _leftBottomPoint.Y) * scaleParametr;
        }

        public void CenterScale(double scaleParametr)
        {
            double width = _rightTopPoint.X - _leftBottomPoint.X;
            double height = _rightTopPoint.Y - _leftBottomPoint.Y;
            double widthAfterScale = width * scaleParametr;
            double heightAfterScale = height * scaleParametr;
            _leftBottomPoint.X -= (widthAfterScale - width) / 2.0;
            _rightTopPoint.X += (widthAfterScale - width) / 2.0;
            _leftBottomPoint.Y -= (heightAfterScale - height) / 2.0;
            _rightTopPoint.Y += (heightAfterScale - height) / 2.0;
        }

        public Rectangle Intersect(Rectangle anotherRectangle)
        {
            double newLeftBottomX = Max(_leftBottomPoint.X, anotherRectangle._leftBottomPoint.X);
            double newLeftBottomY = Max(_leftBottomPoint.Y, anotherRectangle._leftBottomPoint.Y);
            double newRightTopX = Min(_rightTopPoint.X, anotherRectangle._rightTopPoint.X);
            double newRightTopY = Min(_rightTopPoint.Y, anotherRectangle._rightTopPoint.Y);

            if (newLeftBottomX < newRightTopX && newLeftBottomY < newRightTopY)
            {
                return new Rectangle(newLeftBottomX, newLeftBottomY, newRightTopX, newRightTopY);
            }

            throw new InvalidOperationException("There is no rectangle in intersection");
        }

        public Rectangle FindTheLeastRectangle(Rectangle anotherRectangle)
        {
            double newLeftBottomX = Min(_leftBottomPoint.X, anotherRectangle._leftBottomPoint.X);
            double newLeftBottomY = Min(_leftBottomPoint.Y, anotherRectangle._leftBottomPoint.Y);
            double newRightTopX = Max(_rightTopPoint.X, anotherRectangle._rightTopPoint.X);
            double newRightTopY = Max(_rightTopPoint.Y, anotherRectangle._rightTopPoint.Y);
            return new Rectangle(newLeftBottomX, newLeftBottomY, newRightTopX, newRightTopY);
        }

        public override string ToString()
        {
            return string.Format($"Rectangle with left Bottom point in ({_leftBottomPoint.X}, " +
                                 $"{_leftBottomPoint.Y}) " +
                                 $"and right top point in ({_rightTopPoint.X}, {_rightTopPoint.Y})");
        }

        private static double Max(double a, double b)
        {
            return a > b ? a : b;
        }

        private static double Min(double a, double b)
        {
            return a > b ? b : a;
        }
    }
}
